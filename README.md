# Terraform for POC (Proof of concept aws account)
This project has all resources (except the state bucket and admin user) in POC account.
Main purpose is to cover a case study.

### Prerequisites
To able to work on this project:
- AWS account should be [configured](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html).
- The Terraform CLI (v1.0.10+) [installed](https://www.terraform.io/downloads.html).
- The AWS CLI [installed](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html).

### Notes
- State locking is not enabled since this is just a case study.

### How to Run
This project does not have any workspace in it, and it doesn't have any other special requirements.
On your first run you have to initialize working directory and then you can start apply your changes:
```sh
terraform init
terraform apply
```
Before git commit please don't forget to format tf files:
```sh
terraform fmt -recursive
```

### Factorial Calculator:
It's a containerized application which uses a private ECR repository. The container is running on ECS Fargate cluster which is spawned in our private subnet. A public application load balancer targets it. 

You can reach the application by using alb's dns name. My domain's dns server is outside of the AWS account. I've also manually created a cname record for demo.backendlab.com domain. You can also check it like: `http://demo.backendlab.com/?n=3`

Check `factorial_app.tf` file for more information.

### CSV Importer:
It's a serverless lambda application which gets triggered on a CSV upload to S3 bucket and insert the records from CSV into DynamoDB.

In a real world scenario, we may want to reference lambda function source in to S3 bucket. So we can upload the updated artifact directly in the application repository ci/cd.

Check `csv_app.tf` file for more information.