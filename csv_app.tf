resource "aws_dynamodb_table" "customer_table" {
  name         = "Customers"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "Id"
  attribute {
    name = "Id"
    type = "N"
  }
  tags = var.common_tags
}

resource "aws_cloudwatch_log_group" "csv" {
  name              = "/aws/lambda/${aws_lambda_function.csv.function_name}"
  retention_in_days = 7
  tags              = var.common_tags
}

data "aws_iam_policy_document" "csv_logs" {
  statement {
    actions   = ["logs:CreateLogGroup"]
    resources = ["arn:aws:logs:*:${local.account_id}:log-group:/aws/lambda/*"]
  }
  statement {
    actions   = ["logs:CreateLogStream"]
    effect    = "Allow"
    resources = ["${aws_cloudwatch_log_group.csv.arn}:*"]
  }
  statement {
    actions   = ["logs:PutLogEvents"]
    effect    = "Allow"
    resources = ["${aws_cloudwatch_log_group.csv.arn}:log-stream:*"]
  }
}

resource "aws_iam_policy" "csv_logs" {
  name   = "${aws_iam_role.csv.name}PutLogPermission"
  policy = data.aws_iam_policy_document.csv_logs.json
}

resource "aws_iam_role_policy_attachment" "csv_logs" {
  role       = aws_iam_role.csv.name
  policy_arn = aws_iam_policy.csv_logs.arn
}

data "aws_iam_policy_document" "assume_lambda" {
  version = "2012-10-17"
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "csv" {
  name               = "csvLambda"
  assume_role_policy = data.aws_iam_policy_document.assume_lambda.json
  tags = merge(var.common_tags, {
    description = "CSV importer lambda function will use this role"
  })
}

data "aws_iam_policy_document" "csv_s3" {
  version = "2012-10-17"
  statement {
    effect    = "Allow"
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.csv.arn}/*"]
  }
}

resource "aws_iam_policy" "csv_s3" {
  name   = "${aws_iam_role.csv.name}ReadS3Permission"
  policy = data.aws_iam_policy_document.csv_s3.json
}

resource "aws_iam_role_policy_attachment" "csv_s3" {
  role       = aws_iam_role.csv.name
  policy_arn = aws_iam_policy.csv_s3.arn
}

data "aws_iam_policy_document" "csv_dynamodb" {
  version = "2012-10-17"
  statement {
    effect    = "Allow"
    actions   = ["dynamodb:PutItem"]
    resources = [aws_dynamodb_table.customer_table.arn]
  }
}

resource "aws_iam_policy" "csv_dynamodb" {
  name   = "${aws_iam_role.csv.name}PutItemsDynamoDBPermission"
  policy = data.aws_iam_policy_document.csv_dynamodb.json
}

resource "aws_iam_role_policy_attachment" "csv_dynamodb" {
  role       = aws_iam_role.csv.name
  policy_arn = aws_iam_policy.csv_dynamodb.arn
}

resource "aws_s3_bucket" "csv" {
  bucket = "my-customers-as-csv"
  tags = merge(var.common_tags, {
    description = "This bucket keeps all the CSV files of my customers"
  })
}

resource "aws_lambda_function" "csv" {
  function_name    = "csv-importer"
  filename         = var.csv_app_artifact_location
  source_code_hash = filebase64sha256("${var.csv_app_artifact_location}")
  role             = aws_iam_role.csv.arn
  handler          = "main.lambda_handler"
  runtime          = "python3.9"
  memory_size      = 256
  timeout          = 60
  environment {
    variables = {
      TABLE     = aws_dynamodb_table.customer_table.name
      REGION    = local.region
      LOG_LEVEL = "INFO"
    }
  }
  tags       = var.common_tags
  depends_on = [aws_dynamodb_table.customer_table]
}

resource "aws_lambda_permission" "csv_lambda_invoke_over_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.csv.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.csv.arn
}

resource "aws_s3_bucket_notification" "csv_bucket_notification" {
  bucket = aws_s3_bucket.csv.id
  lambda_function {
    lambda_function_arn = aws_lambda_function.csv.arn
    events              = ["s3:ObjectCreated:*"]
    filter_suffix       = ".csv"
  }
  depends_on = [aws_lambda_permission.csv_lambda_invoke_over_bucket]
}