locals {
  factorial_project_name   = "factorial"
  factorial_container_port = 8080
  factorial_task_definition = templatefile("templates/factorial_task_definition.json.tpl", {
    container_name = local.factorial_project_name
    container_port = local.factorial_container_port
    image          = "${aws_ecr_repository.factorial_app.repository_url}:${var.factorial_version}"
    region         = local.region
    log_group_name = aws_cloudwatch_log_group.factorial_app.name
  })
}

resource "aws_ecr_repository" "factorial_app" {
  name                 = local.factorial_project_name
  image_tag_mutability = "IMMUTABLE"
  tags                 = var.common_tags
}

data "aws_iam_policy_document" "factorial_app_pull" {
  version = "2012-10-17"
  statement {
    effect = "Allow"
    actions = [
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy",
      "ecr:DescribeRepositories",
      "ecr:ListImages",
      "ecr:DescribeImages",
      "ecr:BatchGetImage",
      "ecr:GetLifecyclePolicy",
      "ecr:GetLifecyclePolicyPreview",
      "ecr:ListTagsForResource",
      "ecr:DescribeImageScanFindings"
    ]
    resources = [aws_ecr_repository.factorial_app.arn]
  }
  statement {
    effect    = "Allow"
    actions   = ["ecr:GetAuthorizationToken"]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "factorial_app_pull" {
  name   = "AllowECRPull${local.factorial_project_name}"
  policy = data.aws_iam_policy_document.factorial_app_pull.json
}

data "aws_iam_policy_document" "factorial_app_push" {
  version = "2012-10-17"
  statement {
    effect = "Allow"
    actions = [
      "ecr:StartImageScan",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy",
      "ecr:DescribeRepositories",
      "ecr:ListImages",
      "ecr:DescribeImages",
      "ecr:BatchGetImage",
      "ecr:GetLifecyclePolicy",
      "ecr:GetLifecyclePolicyPreview",
      "ecr:ListTagsForResource",
      "ecr:DescribeImageScanFindings",
      "ecr:InitiateLayerUpload",
      "ecr:UploadLayerPart",
      "ecr:CompleteLayerUpload",
      "ecr:PutImage"
    ]
    resources = [aws_ecr_repository.factorial_app.arn]
  }
  statement {
    effect    = "Allow"
    actions   = ["ecr:GetAuthorizationToken"]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "factorial_app_push" {
  name   = "AllowECRPush${local.factorial_project_name}"
  policy = data.aws_iam_policy_document.factorial_app_push.json
}

data "aws_iam_policy_document" "assume_ecs" {
  version = "2012-10-17"
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "factorial_task" {
  name               = "${local.factorial_project_name}Task"
  assume_role_policy = data.aws_iam_policy_document.assume_ecs.json
  tags = merge(var.common_tags, {
    description = "Application container itself will use this role"
  })
}

resource "aws_iam_role" "factorial_execution" {
  name               = "${local.factorial_project_name}Execution"
  assume_role_policy = data.aws_iam_policy_document.assume_ecs.json
  tags = merge(var.common_tags, {
    description = "ECS agent for the application will use this role"
  })
}

resource "aws_iam_role_policy_attachment" "allow_execution_to_put_logs" {
  role       = aws_iam_role.factorial_execution.name
  policy_arn = aws_iam_policy.factorial_app_put_log.arn
}

resource "aws_iam_role_policy_attachment" "allow_execution_to_pull_ecr" {
  role       = aws_iam_role.factorial_execution.name
  policy_arn = aws_iam_policy.factorial_app_pull.arn
}

resource "aws_ecs_cluster" "my_fargate" {
  name = "${local.factorial_project_name}FargateCluster"
  tags = merge(var.common_tags, {
    description = "${local.factorial_project_name} Fargate Cluster"
  })
}

resource "aws_cloudwatch_log_group" "factorial_app" {
  name              = local.factorial_project_name
  retention_in_days = 7
  tags = merge(var.common_tags, {
    description = local.factorial_project_name
  })
}

data "aws_iam_policy_document" "factorial_app_put_log" {
  version = "2012-10-17"
  statement {
    actions   = ["logs:CreateLogStream"]
    effect    = "Allow"
    resources = ["${aws_cloudwatch_log_group.factorial_app.arn}:*"]
  }
  statement {
    actions   = ["logs:PutLogEvents"]
    effect    = "Allow"
    resources = ["${aws_cloudwatch_log_group.factorial_app.arn}:log-stream:*"]
  }
}

resource "aws_iam_policy" "factorial_app_put_log" {
  name   = "${local.factorial_project_name}PutLogPermission"
  policy = data.aws_iam_policy_document.factorial_app_put_log.json
}

resource "aws_security_group" "factorial_app" {
  name   = local.factorial_project_name
  vpc_id = module.vpc.vpc_id

  ingress {
    protocol        = "tcp"
    from_port       = local.factorial_container_port
    to_port         = local.factorial_container_port
    security_groups = [aws_security_group.alb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(var.common_tags, {
    description = "Controls access to ${local.factorial_project_name}"
  })
}

resource "aws_ecs_task_definition" "factorial_app" {
  family                   = local.factorial_project_name
  task_role_arn            = aws_iam_role.factorial_task.arn
  execution_role_arn       = aws_iam_role.factorial_execution.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  container_definitions    = local.factorial_task_definition
  tags = merge(var.common_tags, {
    description = local.factorial_project_name
  })
}

resource "aws_ecs_service" "factorial_app" {
  name            = local.factorial_project_name
  cluster         = aws_ecs_cluster.my_fargate.id
  task_definition = aws_ecs_task_definition.factorial_app.arn
  desired_count   = 1
  launch_type     = "FARGATE"
  network_configuration {
    security_groups  = [aws_security_group.factorial_app.id]
    subnets          = module.vpc.private_subnets
    assign_public_ip = false
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.factorial_app.arn
    container_name   = local.factorial_project_name
    container_port   = local.factorial_container_port
  }
}

resource "aws_lb_target_group" "factorial_app" {
  name                          = local.factorial_project_name
  port                          = local.factorial_container_port
  protocol                      = "HTTP"
  target_type                   = "ip"
  vpc_id                        = module.vpc.vpc_id
  load_balancing_algorithm_type = "least_outstanding_requests"
  health_check {
    unhealthy_threshold = "2"
    healthy_threshold   = "2"
    interval            = "10"
    port                = local.factorial_container_port
    protocol            = "HTTP"
    path                = "/"
    matcher             = "200-299" # range of values
    timeout             = "3"
  }
  tags = merge(var.common_tags, {
    description = local.factorial_project_name
  })
}

resource "aws_security_group" "alb" {
  vpc_id = module.vpc.vpc_id
  name   = "Security group for ALB"
  tags = merge(var.common_tags, {
    description = local.factorial_project_name
  })
}

resource "aws_security_group_rule" "alb_allow_all_egress" {
  security_group_id = aws_security_group.alb.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}

resource "aws_lb" "alb" {
  load_balancer_type = "application"
  name               = "poc-alb"
  security_groups    = [aws_security_group.alb.id]
  subnets            = module.vpc.public_subnets
  internal           = false
  tags = merge(var.common_tags, {
    description = local.factorial_project_name
  })
}

resource "aws_lb_listener" "http_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.factorial_app.arn
  }
}

resource "aws_security_group_rule" "alb_allow_all_ingress_from_http" {
  security_group_id = aws_security_group.alb.id
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}
