[
  {
    "command": [
      "python", "factorial.py"
    ],
    "cpu": 0,
    "entryPoint": [],
    "environment": [
        {
            "name": "PORT",
            "value": "${container_port}"
        }
    ],
    "essential": true,
    "image": "${image}",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "${log_group_name}",
          "awslogs-region": "${region}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "mountPoints": [],
    "name": "${container_name}",
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": ${container_port},
        "hostPort": ${container_port},
        "protocol": "tcp"
      }
    ],
    "secrets": [],
    "volumesFrom": []
  }
]
