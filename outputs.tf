output "factorial_app" {
  value = {
    ecr_url         = aws_ecr_repository.factorial_app.repository_url
    alb_dns_name    = aws_lb.alb.dns_name
    push_policy_arn = aws_iam_policy.factorial_app_push.arn
  }
}
