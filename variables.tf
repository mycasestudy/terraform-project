variable "factorial_version" {
  description = "Docker image version for factorial app to deploy"
  type        = string
  default     = "1.1"
}

variable "csv_app_artifact_location" {
  type        = string
  description = "Absolute path of csv application's artifact. Example: /csv-app-repository/artifacts/build.zip"
}

variable "common_tags" {
  description = "General tags to use in all resources"
  type        = map(string)
  default = {
    terraform = "yes"
    project   = "CaseStudy Terraform"
  }
}
